

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>Profile</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active">Users</li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
      <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box">
            <div class="box-body box-profile">
              <!-- <img class="profile-user-img img-responsive img-circle" src="assets/images/profile.jpg" alt="User profile picture"> -->

              <h3 class="profile-username text-center"><?php echo $user_data['firstname']; ?> <?php echo $user_data['lastname']; ?></h3>

              <p class="text-muted text-center"><?php echo $user_group['group_name']; ?></p>

              <!-- <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul> -->

              <a href="<?php echo base_url('auth/logout') ?>" class="btn btn-warning btn-block"><b>Logout</b></a>
              <!-- <a href=""><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
        </div>
        <div class="col-md-9 col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User Detials</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table ">
                <tr>
                  <th>Username</th>
                  <td><?php echo $user_data['username']; ?></td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td><?php echo $user_data['email']; ?></td>
                </tr>
                <tr>
                  <th>First Name</th>
                  <td><?php echo $user_data['firstname']; ?></td>
                </tr>
                <tr>
                  <th>Last Name</th>
                  <td><?php echo $user_data['lastname']; ?></td>
                </tr>
                <tr>
                  <th>Gender</th>
                  <td><?php echo ($user_data['gender'] == 1) ? 'Male' : 'Gender'; ?></td>
                </tr>
                <tr>
                  <th>Phone</th>
                  <td><?php echo $user_data['phone']; ?></td>
                </tr>
                <tr>
                  <th>Group</th>
                  <!-- <td><span class="label label-info"><//?php echo $user_group['group_name']; ?></span></td> -->
                  <td><?php echo $user_group['group_name']; ?></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    $(document).ready(function() {
      //$('#userTable').DataTable();

      $("#profNav").addClass('active');
      //$("#manageUserNav").addClass('active');
    });
  </script>

 
