<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_Controller 
{
   public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Customers';

		$this->load->model('model_customers');
   }

   /* 
	* It only redirects to the manage customers page
	*/
	public function index()
	{
		if(!in_array('viewCustomers', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$this->render_template('customers/index', $this->data);	
   }

   /*
	* It checks if it gets the customers id and retreives
	* the customers information from the customers model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchCustomersDataById($id) 
	{
		if($id) {
			$data = $this->model_customers->getCustomersData($id);
			echo json_encode($data);
		}

		return false;
   }

   //fetchCustomersData()
   /*
	* Fetches the customer value from the customer table 
	* this function is called from the datatable ajax function
	*/
	public function fetchCustomersData()
	{
		$result = array('data' => array());

		$data = $this->model_customers->getCustomersData();

		foreach ($data as $key => $value) {

			// button
			$buttons = '';

			if(in_array('updateCustomers', $this->permission)) {
				$buttons .= '<button type="button" class="btn btn-default btn-sm" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
			}

			if(in_array('deleteCustomers', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default btn-sm" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
				

			$status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

			$result['data'][$key] = array(
            $value['customer_code'],
            $value['biz_name'],
            $value['name'],
            $value['city'],
            $value['mob_num'],
            $value['segment'],
            // $value['street_home_ofc'],
            // $value['zipcode'],
            $value['term'],
            $value['dsp_visit'],
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
   }
   //create()
   /*
	* Its checks the Customer form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		if(!in_array('createCustomers', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

      $this->form_validation->set_rules('customer_code', 'Customer Code', 'trim|required');
      $this->form_validation->set_rules('biz_name', 'Business Name', 'trim|required');
      $this->form_validation->set_rules('name', 'Name', 'trim|required');
      $this->form_validation->set_rules('city', 'City', 'trim|required');
      $this->form_validation->set_rules('mob_num', 'Contact', 'trim|required');
      $this->form_validation->set_rules('segment', 'Segment', 'trim|required');
      $this->form_validation->set_rules('street_home_ofc', 'Address', 'trim|required');
      $this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');
      $this->form_validation->set_rules('term', 'term', 'trim|required');
      $this->form_validation->set_rules('dsp_visit', 'Visit', 'trim|required');
		$this->form_validation->set_rules('active', 'Active', 'trim|required');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
            'customer_code' => $this->input->post('customer_code'),
            'biz_name' => $this->input->post('biz_name'),
            'name' => $this->input->post('name'),
            'city' => $this->input->post('city'),
            'mob_num' => $this->input->post('mob_num'),
            'segment' => $this->input->post('segment'),
            'street_home_ofc' => $this->input->post('street_home_ofc'),
            'zipcode' => $this->input->post('zipcode'),
            'term' => $this->input->post('term'),
            'dsp_visit' => $this->input->post('dsp_visit'),
            'active' => $this->input->post('active'),	
        	);

        	$create = $this->model_customers->create($data);
        	if($create == true) {
        		$response['success'] = true;
        		$response['messages'] = 'Succesfully created';
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error while creating new customer';			
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}

   //update()
   /*
	* Its checks the customers form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function update($id)
	{

		if(!in_array('updateCustomers', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		if($id) {
         $this->form_validation->set_rules('edit_customer_code', 'Customer Code', 'trim|required');
         $this->form_validation->set_rules('edit_biz_name', 'Business Name', 'trim|required');
         $this->form_validation->set_rules('edit_name', 'Name', 'trim|required');
         $this->form_validation->set_rules('edit_city', 'City', 'trim|required');
         $this->form_validation->set_rules('edit_mob_num', 'Contact', 'trim|required');
         $this->form_validation->set_rules('edit_segment', 'Segment', 'trim|required');
         $this->form_validation->set_rules('edit_street_home_ofc', 'Address', 'trim|required');
         $this->form_validation->set_rules('edit_zipcode', 'Zipcode', 'trim|required');
         $this->form_validation->set_rules('edit_term', 'term', 'trim|required');
         $this->form_validation->set_rules('edit_dsp_visit', 'Visit', 'trim|required');
         $this->form_validation->set_rules('edit_active', 'Active', 'trim|required');

			
			//$this->form_validation->set_rules('edit_active', 'Active', 'trim|required');

			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
					'customer_code' => $this->input->post('edit_customer_code'),
					// 'name' => $this->input->post('edit_name'),
               'biz_name' => $this->input->post('edit_biz_name'),
               'name' => $this->input->post('edit_name'),
               'city' => $this->input->post('edit_city'),
               'mob_num' => $this->input->post('edit_mob_num'),
               'segment' => $this->input->post('edit_segment'),
               'street_home_ofc' => $this->input->post('edit_street_home_ofc'),
               'zipcode' => $this->input->post('edit_zipcode'),
               'term' => $this->input->post('edit_term'),
               'dsp_visit' => $this->input->post('edit_dsp_visit'),
	        		'active' => $this->input->post('edit_active'),	
	        	);

	        	$update = $this->model_customers->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error while updating customer';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}
   
   //Remove()
   /*
	* It removes the customer information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deleteCustomers', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$customers_id = $this->input->post('customers_id');

      $response = array();
      //console.log($response);
		if($customers_id) {
			$delete = $this->model_customers->remove($customers_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error while removing customer";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}
}